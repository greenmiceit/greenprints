module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                files: {
                    'portfolio/css/app.css': 'portfolio/css/app.sass',
                    'shop/css/app.css': 'shop/css/app.sass',
                    'carousel/css/app.css': 'carousel/css/app.sass',
                    'comparison/css/app.css': 'comparison/css/app.sass'
                }
            }
        },
        watch: {
            css: {
                files: '**/*.sass',
                tasks: ['sass']
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['watch']);
};
